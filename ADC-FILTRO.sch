EESchema Schematic File Version 2
LIBS:ADC
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:burr-brown
LIBS:burr-brown-3
LIBS:FUENTE-cache
LIBS:ADC-FILTRO-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date "10 sep 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3100 5350 2550 1300
U 55F1B5D8
F0 "FUENTE" 50
F1 "FUENTE.sch" 50
F2 "+15" I L 3100 5550 60 
F3 "-15V" I L 3100 5750 60 
$EndSheet
$Sheet
S 7050 4100 2150 1350
U 55F1B5F3
F0 "ADC" 50
F1 "ADC.sch" 50
$EndSheet
$Sheet
S 3550 3400 2300 1450
U 55F1B615
F0 "FILTRADO" 50
F1 "FILTRADO.sch" 50
$EndSheet
$Comp
L CONN_2 P?
U 1 1 55F1B631
P 2650 5650
F 0 "P?" V 2600 5650 40  0000 C CNN
F 1 "FUENTE" V 2700 5650 40  0000 C CNN
F 2 "~" H 2650 5650 60  0000 C CNN
F 3 "~" H 2650 5650 60  0000 C CNN
	1    2650 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	3100 5550 3000 5550
Wire Wire Line
	3100 5750 3000 5750
$EndSCHEMATC
